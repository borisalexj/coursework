package com.borisalexj.coursework.ui;

import android.Manifest;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.IntentSender;
import android.content.ServiceConnection;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import com.borisalexj.coursework.R;
import com.borisalexj.coursework.api.ApiCallManager;
import com.borisalexj.coursework.api.iFourSquareApi;
import com.borisalexj.coursework.model.VenueModel;
import com.borisalexj.coursework.orm.MyOrm;
import com.borisalexj.coursework.services.GeolocationService;
import com.borisalexj.coursework.ui.fragments.ListFragment;
import com.borisalexj.coursework.ui.fragments.MapFragment;
import com.borisalexj.coursework.utils.Constants;
import com.borisalexj.coursework.utils.Info;
import com.borisalexj.coursework.utils.Requests;
import com.borisalexj.coursework.utils.StaticHelpers;
import com.borisalexj.coursework.utils.VenuesComparator;
import com.facebook.stetho.Stetho;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStates;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlaceAutocomplete;
import com.google.android.gms.maps.model.LatLng;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;

import okhttp3.OkHttpClient;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

//import com.google.android.gms.appindexing.AppIndex;

public class MainActivity extends MyCustomAppCompatActivity {

    private String TAG = Info.TAG + this.getClass().getSimpleName();
    private View mSearchOptionsView;
    private Button mSearchOptionsButton;
    private RadioButton mUseGpsLocationRadio;

    CompoundButton.OnCheckedChangeListener useCustomLocationRadioCheckedChangeListener = new CompoundButton.OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
            Log.d(TAG, "onCheckedChanged: ");
            if (isChecked) {
                mUseGpsLocationRadio.setChecked(false);
//                useCustomLocationRadioClick();
            }

        }
    };
    private RadioButton mUseCustomLocationRadio;
    private CheckBox mAutoupdateOnLocationChanged;
    private String mPlaceApiAddress;
    private LatLng mPlaceApiLatLng;
    private MyOrm mMyOrm;
    private android.support.v4.app.FragmentManager mFragmentManager;
    private MyCustomFragment[] mFragments;
    private MyCustomFragment mCurrentFragment;
    CompoundButton.OnCheckedChangeListener useGpsLocationRadioCheckedChangeListener = new CompoundButton.OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
            Log.d(TAG, "onCheckedChanged: ");
            if (isChecked) {
                mUseCustomLocationRadio.setChecked(false);
                useGpsLocationRadioClick();
            }
        }
    };
    private BroadcastReceiver broadcastReceiver;
    private boolean mBound = false;
    private Messenger mService;
    private Location mLastLocation;
    private boolean mGotLocation = false;
    private boolean mFirstLocationChange = true;
    private boolean mHasLocationPermission = false;
    private ServiceConnection mConnection = new ServiceConnection() {
        public void onServiceConnected(ComponentName className, IBinder service) {
            // This is called when the connection with the service has been
            // established, giving us the object we can use to
            // interact with the service.  We are communicating with the
            // service using a Messenger, so here we get a client-side
            // representation of that from the raw IBinder object.
            mService = new Messenger(service);
            mBound = true;
            initializeLocationServices();
        }

        public void onServiceDisconnected(ComponentName className) {
            // This is called when the connection with the service has been
            // unexpectedly disconnected -- that is, its process crashed.
            mService = null;
            mBound = false;
        }
    };
    private ApiCallManager mApiCallManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.d(TAG, "onCreate: ");
        super.onCreate(savedInstanceState);
        Stetho.initializeWithDefaults(this);

//        getIntent();
//        android.R.array.emailAddressTypes
//        android.R.fraction
//        android.R.id.content
//Intent.FLAG_ACTIVITY_NO_ANIMATION;
//        getSupportFragmentManager().beginTransaction().setBreadCrumbTitle()
//        getSupportFragmentManager().beginTransaction().commitAllowingStateLoss()
        setContentView(R.layout.activity_main);

        mApiCallManager = new ApiCallManager(getApplicationContext());

        mFragmentManager = getSupportFragmentManager();
        mFragments = new MyCustomFragment[]{new ListFragment(), new MapFragment()};
        mSearchOptionsView = findViewById(R.id.searchOptionsView);

        mAutoupdateOnLocationChanged = (CheckBox) findViewById(R.id.autoupdateOnLocationChanged);
        mUseGpsLocationRadio = (RadioButton) findViewById(R.id.useGpsLocationRadio);
        mUseCustomLocationRadio = (RadioButton) findViewById(R.id.useCustomLocationRadio);

        mSearchOptionsButton = (android.widget.Button) findViewById(R.id.searchOptionsButton);

        mUseGpsLocationRadio.setOnCheckedChangeListener(useGpsLocationRadioCheckedChangeListener);
        mUseCustomLocationRadio.setOnCheckedChangeListener(useCustomLocationRadioCheckedChangeListener);
        mUseCustomLocationRadio.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                useCustomLocationRadioClick();
            }
        });

        mMyOrm = new MyOrm(getApplicationContext());

        showListFragment();

        mSearchOptionsView.setVisibility(View.GONE);

    }

    @Override
    protected void onStart() {
        super.onStart();
        // Bind to GeolocationService
        bindService(new Intent(this, GeolocationService.class), mConnection,
                Context.BIND_AUTO_CREATE);
        initializeBroadcastReceiver();

        checkPermisionsAndStartLocationService();
    }

    private void initializeBroadcastReceiver() {
        broadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                Log.d(TAG, "onReceive: inside receiver");
                if (intent.getAction().equals(Constants.INTENT_GOT_LOCATION)) {
                    mLastLocation = intent.getParcelableExtra("location");
                    if (mLastLocation == null) {
                        return;
                    }
                    Log.d(TAG, "onReceive: " + mLastLocation);
                    mGotLocation = true;
                    if (mAutoupdateOnLocationChanged.isChecked() || mFirstLocationChange) {
                        getCurrentFragment().onLocationChanged();
                    }
                    mFirstLocationChange = false;
                    Toast.makeText(MainActivity.this, "gps\nlat:" + mLastLocation.getLatitude() + "\nlng:" + mLastLocation.getLongitude(), Toast.LENGTH_SHORT).show();
                }

            }
        };
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(Constants.INTENT_GOT_LOCATION);
        LocalBroadcastManager.getInstance(this).registerReceiver(broadcastReceiver, intentFilter);

    }

    @Override
    protected void onStop() {
        super.onStop();
        super.onStop();
        // Unbind from the service
        if (mBound) {
            unbindService(mConnection);
            mBound = false;
        }
    }

    private void checkPermisionsAndStartLocationService() {
        makeRequestForGpsTurnedOn();

        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            Log.d(TAG, "onPostResume: permission denied");
            if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.ACCESS_FINE_LOCATION) ||
                    ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.ACCESS_COARSE_LOCATION)) {
                Log.d(TAG, "onPostResume: should show toast");
                Toast.makeText(getApplicationContext(), "GPS permission allows us to access location data. Please allow in App PppSettings for additional functionality.", Toast.LENGTH_LONG).show();
            } else {
                Log.d(TAG, "onPostResume: should request permission");
            }
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION}, Requests.REQUEST_LOCATION_PERMISSION);

            return;
        } else {
            mHasLocationPermission = true;
            initializeLocationServices();
        }

    }

    public void useGpsLocationRadioClick() {
        Log.d(TAG, "useGpsLocationRadioClick: ");
        getCurrentFragment().onLocationChanged();
    }

    public void useCustomLocationRadioClick() {
        Log.d(TAG, "useCustomLocationRadioClick: ");

        try {
            Intent intent =
                    new PlaceAutocomplete.IntentBuilder(PlaceAutocomplete.MODE_FULLSCREEN)
                            .build(this);
            startActivityForResult(intent, Requests.PLACE_AUTOCOMPLETE_REQUEST_CODE);
        } catch (GooglePlayServicesRepairableException e) {
            // TODO: Handle the error.
            e.printStackTrace();
            Log.d(TAG, "useCustomLocationRadioClick: GooglePlayServicesRepairableException");
        } catch (GooglePlayServicesNotAvailableException e) {
            // TODO: Handle the error.
            e.printStackTrace();
            Log.d(TAG, "useCustomLocationRadioClick: GooglePlayServicesNotAvailableException");
        }
    }

    public void makeFourSquareRequest(double latitude, double longitude, int _limit, int _radius) {
        Log.d(TAG, "makeFourSquareRequest: ");

        if (latitude == 0 && longitude == 0) {
            Log.d(TAG, "makeFourSquareRequest: doubleZero income");
        }

        if (latitude == -1 && longitude == -1) {
            if (mLastLocation != null && mUseGpsLocationRadio.isChecked()) {
                latitude = mLastLocation.getLatitude();
                longitude = mLastLocation.getLongitude();
            } else if (mPlaceApiLatLng != null && mUseCustomLocationRadio.isChecked()) {
                latitude = mPlaceApiLatLng.latitude;
                longitude = mPlaceApiLatLng.longitude;
            } else {
                Log.d(TAG, "makeFourSquareRequest: doubleZero set");
                Toast.makeText(this, "Unfortunately, you don't have any coordinates 1", Toast.LENGTH_SHORT).show();
                return;
            }
        }

        String ll = String.valueOf(latitude + "," + longitude);
        String limit;
        String radius;

        ArrayList<VenueModel> venues;

        if (_limit != -1) { //список
            limit = String.valueOf(_limit);
            radius = null;
        } else if (_radius != -1) { // карта
            radius = String.valueOf(_radius);
            limit = null;
        } else {
            Log.e(TAG, "makeFourSquareRequest: unknown call");
            return;
        }

        venues = mApiCallManager.GetVenues(ll, limit, radius);
        if (venues == null) {
            Toast.makeText(MainActivity.this, "Turn internet \"ON\" to have more recent data. \n Currently using cache.", Toast.LENGTH_SHORT).show();
            venues = mMyOrm.readAllVenues();
            Log.d(TAG, "onFailure: " + venues.size());
        } else {
            Log.d(TAG, "onResponse: " + venues.size());
            mMyOrm.storeVenues(venues);
            venues = StaticHelpers.setCustomRates(venues, mMyOrm.getRatesArray());
        }

        if (venues.size() == 0) {
            return; // todo - сделать чего-то с пустым ответом.
        }

        if (mLastLocation != null) {
            LatLng currentLatLng = new LatLng(mLastLocation.getLatitude(), mLastLocation.getLongitude());
            venues = StaticHelpers.recalculateDistances(venues, currentLatLng);
        }
        if (_limit != -1) { // список
            Collections.sort(venues, new VenuesComparator());
            if (_limit < venues.size()) {
                venues = StaticHelpers.subList(venues, _limit);
            }
        } else if (_radius != -1) { // карта
            // todo - переделать на границы карты запрос и базу
            venues = StaticHelpers.weedWithinRadius(venues, _radius);
        }

        venues = StaticHelpers.markHighestRated(venues);
        getCurrentFragment().showVenues(venues);
    }

    public void makeFourSquareRequestAsync(double latitude, double longitude, final int _limit, final int _radius) {

        if (latitude == 0 && longitude == 0) {
            Log.d(TAG, "makeFourSquareRequest: doubleZero income");
        }

        if (latitude == -1 && longitude == -1) {
            if (mLastLocation != null && mUseGpsLocationRadio.isChecked()) {
                latitude = mLastLocation.getLatitude();
                longitude = mLastLocation.getLongitude();
            } else if (mPlaceApiLatLng != null && mUseCustomLocationRadio.isChecked()) {
                latitude = mPlaceApiLatLng.latitude;
                longitude = mPlaceApiLatLng.longitude;
            } else {
                Log.d(TAG, "makeFourSquareRequest: doubleZero set");
                Toast.makeText(this, "Unfortunately, you don't have coordinates 1", Toast.LENGTH_SHORT).show();
                return;
            }
        }
        Log.d(TAG, "makeFourSquareRequest: ");

        final String API_BASE_URL = getString(R.string.foursquare_api_base_url);
        final String API_METHOD_URL = getString(R.string.foursquare_api_method_venues_search);
        String v = getString(R.string.foursquare_v);
        String intent = getString(R.string.foursquare_intent);
        String categoryId = getString(R.string.foursquare_category_id);
        String client_id = getString(R.string.foursquare_client_id);
        String client_secret = getString(R.string.foursquare_client_secret);
        String ll = String.valueOf(latitude + "," + longitude);
        final String limit = String.valueOf(_limit);
        String radius = String.valueOf(_radius);
        String m = getString(R.string.foursquare_m);

        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();

        Retrofit.Builder builder =
                new Retrofit.Builder()
//                        .callbackExecutor(Executors.newSingleThreadExecutor())
//                        .addConverterFactory(GsonConverterFactory.create())
                        .baseUrl(API_BASE_URL);

        Retrofit retrofit = builder.client(httpClient.build()).build();
        iFourSquareApi client = retrofit.create(iFourSquareApi.class);

        Log.d(TAG, "makeFourSquareRequest: " +
                API_BASE_URL +
                API_METHOD_URL + "?" +
                "v=" + v + "&" +
                "categoryId=" + categoryId + "&" +
                "client_id=" + client_id + "&" +
                "client_secret=" + client_secret + "&" +
                "ll=" + ll + "&" +
                "limit=" + limit + "&" +
                "radius=" + radius + "&" +
                "m=" + m);

        Call<ResponseBody> call;

        if (_limit != -1) { //список
            call = client.searchVenuesForList(API_METHOD_URL, v, categoryId, client_id, client_secret, ll, limit, m);
        } else if (_radius != -1) { // карта
            call = client.searchVenuesForMap(API_METHOD_URL, v, categoryId, client_id, client_secret, ll, radius, m);
        } else {
            Log.e(TAG, "makeFourSquareRequest: unknown call");
            return;
        }

        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                Log.d(TAG, "onResponse: success");
                JSONObject result = StaticHelpers.responseToJson(response);

                try {
                    ArrayList<VenueModel> venues = StaticHelpers.parseVenuesSearchResponse(result);
                    if (venues.size() == 0) {
                        return;
                    }
                    if (mLastLocation != null) {
                        LatLng currentLatLng = new LatLng(mLastLocation.getLatitude(), mLastLocation.getLongitude());
                        venues = StaticHelpers.recalculateDistances(venues, currentLatLng);
                    }
                    Collections.sort(venues, new VenuesComparator());
                    mMyOrm.storeVenues(venues);
                    venues = StaticHelpers.setCustomRates(venues, mMyOrm.getRatesArray());
                    venues = StaticHelpers.markHighestRated(venues);
                    getCurrentFragment().showVenues(venues);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

                Log.d(TAG, "onFailure: " + call.request().url());
                Log.d(TAG, "onFailure: " + t);

                Toast.makeText(MainActivity.this, "Turn internet \"ON\" to have more recent data. \n Currently using cache.", Toast.LENGTH_SHORT).show();

                ArrayList<VenueModel> venues = new ArrayList<>();

                venues = mMyOrm.readAllVenues();
                Log.d(TAG, "onFailure: " + venues.size());

                if (venues.size() == 0) {
                    return;
                }

                if (mLastLocation != null) {
                    LatLng currentLatLng = new LatLng(mLastLocation.getLatitude(), mLastLocation.getLongitude());
                    venues = StaticHelpers.recalculateDistances(venues, currentLatLng);
                }

                if (_limit != -1) { // список
                    Collections.sort(venues, new VenuesComparator());
                    if (_limit < venues.size()) {
                        venues = StaticHelpers.subList(venues, _limit);
                    }

                } else if (_radius != -1) { // карта
                    venues = StaticHelpers.weedWithinRadius(venues, _radius);
                }
                venues = StaticHelpers.markHighestRated(venues);
                getCurrentFragment().showVenues(venues);
            }
        });
    }

    @Override
    public boolean isGotGpsLocation() {
        Log.d(TAG, "isGotGpsLocation: " + mGotLocation);
        return mGotLocation;
    }

    @Override
    public boolean isGotLocation() {
        Log.d(TAG, "isGotGpsLocation: " + mGotLocation);
        if (mUseGpsLocationRadio.isChecked()) {
            return mGotLocation;
        } else if (mUseCustomLocationRadio.isChecked() && mPlaceApiLatLng != null) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public LatLng getGpsLocation() {
        Log.d(TAG, "getGpsLocation: ");
        if (mLastLocation != null) {
            Log.d(TAG, "getGpsLocation: " + mLastLocation);
            return new LatLng(mLastLocation.getLatitude(), mLastLocation.getLongitude());
        } else {
            Log.d(TAG, "getGpsLocation: doubleZero set");
            Toast.makeText(this, "Unfortunately, you don't have coordinates 2", Toast.LENGTH_SHORT).show();
            return new LatLng(0, 0);
        }
    }

    @Override
    public LatLng getLocation() {
        if (mLastLocation != null && mUseGpsLocationRadio.isChecked()) {
            Log.d(TAG, "getLocation: " + mLastLocation);
            return new LatLng(mLastLocation.getLatitude(), mLastLocation.getLongitude());
        } else if (mPlaceApiLatLng != null && mUseCustomLocationRadio.isChecked()) {
            Log.d(TAG, "getLocation: " + mPlaceApiLatLng);
            return mPlaceApiLatLng;
        } else {
            Log.d(TAG, "getLocation: doubleZero set");
            Toast.makeText(this, "Unfortunately, you don't have coordinates 3", Toast.LENGTH_SHORT).show();
            return new LatLng(0, 0);
        }
    }


    @Override
    public void showInfoDialogue(VenueModel vm) {
        requestVenueInfo(vm);
    }

    public void requestVenueInfo(VenueModel vm) {
        Log.d(TAG, "requestVenueInfo: ");
        String venuePrice = mApiCallManager.GetVenueDetail(vm.getIdVenue());

        if (venuePrice != null) {
            Log.d(TAG, "onResponse: success");
            vm.setPrice(venuePrice);
        } else {
            Log.d(TAG, "onFailure: ");
            Toast.makeText(MainActivity.this, "Turn internet \"ON\" to have more recent data. \n Currently using cache.", Toast.LENGTH_SHORT).show();
            vm.setPrice(String.valueOf("price unknown"));
        }
        showInfoDialogueFinally(vm);
    }


    public void showInfoDialogueFinally(final VenueModel vm) {
        Dialog dialog;
        LayoutInflater inflater = getLayoutInflater();
        View dialoglayout = inflater.inflate(R.layout.dialog_info, null);

        TextView venueNameTextView = (TextView) dialoglayout.findViewById(R.id.venueNameTextView);
        TextView venuePhoneTextView = (TextView) dialoglayout.findViewById(R.id.venuePhoneTextView);
        TextView venueAddressTextView = (TextView) dialoglayout.findViewById(R.id.venueAddressTextView);
        TextView venuePriceTextView = (TextView) dialoglayout.findViewById(R.id.venuePriceTextView);
        final EditText venueRateEditText = (EditText) dialoglayout.findViewById(R.id.venueRateEditText);
        EditText userNicknameEditText = (EditText) dialoglayout.findViewById(R.id.userNicknameEditText);

        venueNameTextView.setText(vm.getName());
        venuePhoneTextView.setText(vm.getFormattedPhone());
        venueAddressTextView.setText(vm.getAddress());
        venuePriceTextView.setText(vm.getPrice());
        venueRateEditText.setText(String.valueOf(vm.getCustomUserRate()));

        android.support.v7.app.AlertDialog.Builder dialogBuilder = new android.support.v7.app.AlertDialog.Builder(this);

        dialogBuilder.setView(dialoglayout);
        dialogBuilder.setTitle("Venue info:");
        dialogBuilder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Log.d(TAG, "onClick: setPositiveButton");
                if (!venueRateEditText.getText().equals(vm.getCustomUserRate())) {
                    mMyOrm.updateUserCustomRate(vm.getIdVenue(), String.valueOf(venueRateEditText.getText()));
                    getCurrentFragment().initialRequestData();
                }

            }
        });

        dialogBuilder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Log.d(TAG, "onClick: setNegativeButton");
            }
        });

        dialog = dialogBuilder.create();
        dialog.getWindow().getAttributes().windowAnimations = R.style.DialogSlideAnim;

        dialog.show();
    }

    public void requestVenueInfoAsync(final VenueModel vm) {

        Log.d(TAG, "requestVenueInfo: ");
        final String API_BASE_URL = "https://api.foursquare.com/";
        final String API_METHOD_URL = "/v2/venues/" + vm.getIdVenue();

        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        Retrofit.Builder builder =
                new Retrofit.Builder()
//                        .callbackExecutor(Executors.newSingleThreadExecutor())
//                        .addConverterFactory(GsonConverterFactory.create())
                        .baseUrl(API_BASE_URL);

        Retrofit retrofit = builder.client(httpClient.build()).build();
        iFourSquareApi client = retrofit.create(iFourSquareApi.class);

        String v = getString(R.string.foursquare_v);
        String intent = getString(R.string.foursquare_intent);
        String client_id = getString(R.string.foursquare_client_id);
        String client_secret = getString(R.string.foursquare_client_secret);

        Log.d(TAG, "makeFourSquareRequest: " +
                API_BASE_URL +
                API_METHOD_URL +
                "v=" + v + "&" +
                "client_id=" + client_id + "&" +
                "client_secret=" + client_secret + "&");

        Call<ResponseBody> call;

        call = client.venuesDetail(API_METHOD_URL, v, client_id, client_secret);

        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                Log.d(TAG, "onResponse: success");

                JSONObject result = StaticHelpers.responseToJson(response);
//                Gson gson = new GsonBuilder().setLenient().create();
                try {
//                    Log.d(TAG, "onResponse: " + response);
//                    Log.d(TAG, "onResponse: " + response.body());
//                    result = new JSONObject(gson.toJson(response.body()));

                    vm.setPrice(String.valueOf(result.getJSONObject("response").getJSONObject("venue").getJSONObject("price").getInt("tier")));

                } catch (JSONException e) {
                    vm.setPrice("Price unknown");
                    e.printStackTrace();
                }
                showInfoDialogueFinally(vm);
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.d(TAG, "onFailure: ");
                Log.d(TAG, "onFailure: " + call.request().url());
                Log.d(TAG, "onFailure: " + t);
                Toast.makeText(MainActivity.this, "Turn internet \"ON\" to have more recent data. \n Currently using cache.", Toast.LENGTH_SHORT).show();
                vm.setPrice(String.valueOf("price unknown"));
                showInfoDialogueFinally(vm);
            }
        });
    }

    public MyCustomFragment getCurrentFragment() {
        Log.d(TAG, "getCurrentFragment: ");
        if (mFragmentManager.getFragments().size() == 0) {
            showListFragment();
        }
        return mCurrentFragment;
    }

    public void showListFragment() {
        Log.d(TAG, "showListFragment: ");
        mCurrentFragment = mFragments[0];
        mFragmentManager.beginTransaction()
                .replace(R.id.mainForFragmentsLayout, mCurrentFragment, "frgmnt")
                .commit();
    }

    public void showMapFragment() {
        Log.d(TAG, "showMapFragment: ");
        mCurrentFragment = mFragments[1];
        mFragmentManager.beginTransaction()
                .replace(R.id.mainForFragmentsLayout, mCurrentFragment, "frgmnt")
                .commit();
    }

    public void showListButtonClick(View view) {
        Log.d(TAG, "showListButtonClick: ");
        showListFragment();
    }

    public void showMapButtonClick(View view) {
        Log.d(TAG, "showMapButtonClick: ");
        showMapFragment();
    }

    public void searchOptionsButtonClick(View view) {
        Log.d(TAG, "searchOptionsButtonClick: ");

        Animation locationTabAnimation;

        if (mSearchOptionsView.getVisibility() == View.GONE) {
            mSearchOptionsButton.setBackgroundColor(ContextCompat.getColor(this, R.color.selectedButton));
            mSearchOptionsView.setVisibility(View.VISIBLE);

            locationTabAnimation = AnimationUtils.loadAnimation(this, R.anim.set_location_shown_anim);
            locationTabAnimation.setRepeatCount(1);
            mSearchOptionsView.startAnimation(locationTabAnimation);
        } else {
            mSearchOptionsButton.setBackgroundColor(ContextCompat.getColor(this, R.color.unSelectedButton));

            locationTabAnimation = AnimationUtils.loadAnimation(this, R.anim.set_location_hidden_anim);
            locationTabAnimation.setRepeatCount(1);
            mSearchOptionsView.startAnimation(locationTabAnimation);
            view.postDelayed(new Runnable() {
                @Override
                public void run() {
                    mSearchOptionsView.setVisibility(View.GONE);
                }
            }, 250);
            getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
            View viewForFocus = this.getCurrentFocus();
            if (viewForFocus != null) {
                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(viewForFocus.getWindowToken(), 0);
            }
        }

    }

    @Override
    protected void onResume() {
        Log.d(TAG, "onResume: ");
        super.onResume();
    }

    public void makeRequestForGpsTurnedOn() {
        Log.d(TAG, "makeRequestForGpsTurnedOn: ");
        GoogleApiClient googleApiClient;

        googleApiClient = new GoogleApiClient.Builder(getApplicationContext()).addApi(LocationServices.API).build();
        googleApiClient.connect();

        LocationRequest locationRequest = LocationRequest.create();
        locationRequest.setInterval(Constants.LOCATION_INTERVAL);
        locationRequest.setFastestInterval(Math.round(Constants.LOCATION_INTERVAL) / 2);
        locationRequest.setExpirationTime(Constants.LOCATION_INTERVAL);
        locationRequest.setExpirationDuration(Constants.LOCATION_INTERVAL);
        locationRequest.setSmallestDisplacement(Constants.LOCATION_DISTANCE);
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);

        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder().addLocationRequest(locationRequest);
        builder.setAlwaysShow(true); // this is the key ingredient

        PendingResult<LocationSettingsResult> result = LocationServices.SettingsApi.checkLocationSettings(googleApiClient, builder.build());
        result.setResultCallback(
                new ResultCallback<LocationSettingsResult>() {
                    @Override
                    public void onResult(@NonNull LocationSettingsResult result) {

                        final Status status = result.getStatus();
                        final LocationSettingsStates state = result.getLocationSettingsStates();
                        switch (status.getStatusCode()) {
                            case LocationSettingsStatusCodes.SUCCESS:
                                mHasLocationPermission = true;
                                initializeLocationServices();
                                break;
                            case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                                try {
                                    status.startResolutionForResult(MainActivity.this, Requests.REQUEST_LOCATION_TURN_ON);
                                } catch (IntentSender.SendIntentException e) {
                                }
                                break;
                            case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                                break;
                        }
                    }
                }
        );
//        googleApiClient = new GoogleApiClient.Builder(this).addApi(AppIndex.API).build();
        googleApiClient.disconnect();
    }

    @Override
    protected void onPause() {
        Log.d(TAG, "onPause: ");
        super.onPause();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        Log.d(TAG, "onRequestPermissionsResult: ");
        switch (requestCode) {
            case Requests.REQUEST_LOCATION_PERMISSION:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Log.d(TAG, "onRequestPermissionsResult: granted");
                    mHasLocationPermission = true;
                    initializeLocationServices();
                } else {
                    Log.d(TAG, "onRequestPermissionsResult: NOT granted");
                    Toast.makeText(MainActivity.this, "Permission Denied, You cannot access location data.", Toast.LENGTH_SHORT).show();
                }
                break;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        Log.d("onActivityResult()", Integer.toString(resultCode));

        switch (requestCode) {
            case Requests.PLACE_AUTOCOMPLETE_REQUEST_CODE: {
                if (resultCode == RESULT_OK) {
                    Place place = PlaceAutocomplete.getPlace(this, data);
                    Log.i(TAG, "Place: " + place.getName() + " " + place.getAddress());
                    mPlaceApiAddress = String.valueOf(place.getAddress());
                    mPlaceApiLatLng = place.getLatLng();
                    getCurrentFragment().onLocationChanged();
                    Toast.makeText(this, place.getName(), Toast.LENGTH_SHORT).show();
                } else if (resultCode == PlaceAutocomplete.RESULT_ERROR) {
                    Status status = PlaceAutocomplete.getStatus(this, data);
                    // TODO: Handle the error.
                    Log.i(TAG, status.getStatusMessage());
                    Toast.makeText(this, status.getStatusMessage(), Toast.LENGTH_SHORT).show();

                } else if (resultCode == RESULT_CANCELED) {
                    // The user canceled the operation.
                    Log.i(TAG, "PLACE_AUTOCOMPLETE_REQUEST_CODE - RESULT_CANCELED");
                    mUseGpsLocationRadio.setChecked(true);
                }
                break;

            }

            case Requests.REQUEST_LOCATION_TURN_ON: {
                switch (resultCode) {
                    case RESULT_OK: {
                        initializeLocationServices();
                        Toast.makeText(this, "Location enabled by user!", Toast.LENGTH_LONG).show();
                        break;
                    }
                    case RESULT_CANCELED: {
                        Toast.makeText(this, "Location not enabled, user cancelled.", Toast.LENGTH_LONG).show();

                        break;
                    }
                    default: {
                        break;
                    }
                }
                break;
            }
        }
    }

    private void initializeLocationServices() {
        if (mBound && mHasLocationPermission) {
            Message msg = Message.obtain(null, Constants.MSG_START_GEOLOCATION, 0, 0);
            try {
                mService.send(msg);
            } catch (RemoteException e) {
                e.printStackTrace();
            }
        }
    }

}
