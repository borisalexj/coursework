package com.borisalexj.coursework.ui.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.borisalexj.coursework.R;
import com.borisalexj.coursework.model.VenueModel;
import com.borisalexj.coursework.ui.InterfaceForActivity;
import com.borisalexj.coursework.ui.MyCustomAppCompatActivity;
import com.borisalexj.coursework.ui.MyCustomFragment;
import com.borisalexj.coursework.utils.Constants;
import com.borisalexj.coursework.utils.Info;

import java.util.ArrayList;

/**
 * Created by user on 3/17/2017.
 */

public class ListFragment extends MyCustomFragment {

    private String TAG = Info.TAG + this.getClass().getSimpleName();
    private ListView mListView;
    private ArrayList<VenueModel> mVenuesList = new ArrayList<VenueModel>();
    private VenueListAdapter mAdapter;
    private VenueModel venue = new VenueModel();

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        Log.d(TAG, "onCreateView: ");
        View view = inflater.inflate(R.layout.fragment_list, container, false);
        ((Button) getActivity().findViewById(R.id.mainListButton)).setBackgroundColor(ContextCompat.getColor(getContext(), R.color.selectedButton));
        mListView = (ListView) view.findViewById(R.id.listView);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity().getBaseContext());

        mAdapter = new VenueListAdapter(mVenuesList, getContext());
        mListView.setAdapter(mAdapter);
//        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
//            @Override
//            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
//                Toast.makeText(getContext(),
//                        String.format("%s\n%s\n%s\n%s", parent.hashCode(), view.getId(), position, id), Toast.LENGTH_SHORT).show();
//            }
//        });

        if (((InterfaceForActivity) getActivity()).isGotGpsLocation()) {
            initialRequestData();
        }

        return view;
    }

    @Override
    public void onResume() {
        Log.d(TAG, "onResume: ");
        super.onResume();
        mListView.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {
                Log.d(TAG, "onScrollStateChanged: ");
                if (scrollState == AbsListView.OnScrollListener.SCROLL_STATE_IDLE
                        && (mListView.getLastVisiblePosition() - mListView.getHeaderViewsCount() -
                        mListView.getFooterViewsCount()) >= (mListView.getCount() - 1)) {
                    // Now your listview has hit the bottom
                    ((MyCustomAppCompatActivity) getActivity()).makeFourSquareRequest(-1, -1, mAdapter.getCount() + Constants.START_ITEM_COUNT, -1);
                }
            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {

            }
        });
    }

    @Override
    public void onDestroy() {
        Log.d(TAG, "onDestroy: ");
        super.onDestroy();
        ((Button) getActivity().findViewById(R.id.mainListButton)).setBackgroundColor(ContextCompat.getColor(getContext(), R.color.unSelectedButton));
    }

    @Override
    public void showVenues(ArrayList<VenueModel> venuesList) {
        Log.d(TAG, "showVenues: " + venuesList.size());
        mVenuesList = venuesList;
        mAdapter.setVenueArray(venuesList);
        mAdapter.notifyDataSetChanged();
    }

    public void requestActivityToShowDialogue(VenueModel vm) {
        ((MyCustomAppCompatActivity) getActivity()).showInfoDialogue(vm);
    }

    @Override
    public void initialRequestData() {
        Log.d(TAG, "initialRequestData: ");

        int itemCount = mAdapter.getCount() == 0 ? Constants.START_ITEM_COUNT : mAdapter.getCount();
        ((MyCustomAppCompatActivity) getActivity()).makeFourSquareRequest(-1, -1, itemCount, -1);
    }

    @Override
    public void onLocationChanged() {
        Log.d(TAG, "onLocationChanged: ");
        initialRequestData();
    }

    public class VenueListAdapter extends BaseAdapter {
        public Context context;
        ArrayList<VenueModel> mVenueArray;

        public VenueListAdapter(ArrayList<VenueModel> venueArray, Context context) {
            this.mVenueArray = venueArray;
            this.context = context;
        }

        public void setVenueArray(ArrayList<VenueModel> venueArray) {
            mVenueArray = venueArray;
        }

        @Override
        public int getCount() {
            return mVenueArray.size();
        }

        @Override
        public VenueModel getItem(int position) {
            return mVenueArray.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(final int position, View view, ViewGroup parent) {
            LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = layoutInflater.inflate(R.layout.venue_list_item, parent, false);

            venue = getItem(position);

            ((TextView) view.findViewById(R.id.venueNameTextView)).setText(getString(R.string.venue_label, venue.getName()));
            ((TextView) view.findViewById(R.id.distanceToVenueTextView)).setText(getString(R.string.distance_label, String.valueOf(venue.getDistance())));

            view.setOnClickListener(new View.OnClickListener() {
                VenueModel _vm = venue;

                @Override
                public void onClick(View v) {
                    requestActivityToShowDialogue(_vm);
                }
            });

            return view;
        }
    }
}
