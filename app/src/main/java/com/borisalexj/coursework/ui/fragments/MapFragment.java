package com.borisalexj.coursework.ui.fragments;

import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.DrawableRes;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.borisalexj.coursework.R;
import com.borisalexj.coursework.model.VenueModel;
import com.borisalexj.coursework.ui.MyCustomAppCompatActivity;
import com.borisalexj.coursework.ui.MyCustomFragment;
import com.borisalexj.coursework.utils.Constants;
import com.borisalexj.coursework.utils.Info;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.maps.android.SphericalUtil;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by user on 3/17/2017.
 */

public class MapFragment extends MyCustomFragment {
    private String TAG = Info.TAG + this.getClass().getSimpleName();
    private GoogleMap mGoogleMap;
    private MapView mMapView;
    private LayoutInflater mLayoutInflater;
    private Marker mLastOpened = null;
    private LatLng mMapCenter;
    private LatLngBounds mMapBounds;
    private int mMapRadius = 3000;
    private GoogleMap.OnCameraChangeListener mCustomOnCameraChangeListener = new GoogleMap.OnCameraChangeListener() {
        @Override
        public void onCameraChange(CameraPosition cameraPosition) {

            mMapCenter = cameraPosition.target;
            mMapBounds = mGoogleMap.getProjection().getVisibleRegion().latLngBounds;

            mMapRadius = (int) Math.round(SphericalUtil.computeDistanceBetween(mMapBounds.northeast, mMapBounds.southwest) / 2);
            Log.d(TAG, "onCameraChange: " + mMapRadius);

            if (((MyCustomAppCompatActivity) getActivity()).isGotLocation()) {
                initialRequestData();
            }
        }
    };
    private HashMap<LatLng, VenueModel> mLigamentMarkerVenue = new HashMap();
    private GoogleMap.OnMarkerClickListener mCustomOnMarkerClickListener = new GoogleMap.OnMarkerClickListener() {
        @Override
        public boolean onMarkerClick(Marker marker) {
            // Check if there is an open info window

            if (mLastOpened != null) {
                // Close the info window
                mLastOpened.hideInfoWindow();

                // Is the marker the same marker that was already open
                if (mLastOpened.equals(marker)) {
                    // Nullify the mLastOpened object
                    mLastOpened = null;
                    // Return so that the info window isn't openned again
                    return true;
                }
            }

            // Open the info window for the marker
            marker.showInfoWindow();
            // Re-assign the last openned such that we can close it later
            mLastOpened = marker;

            // search in mLigamentMarkerVenue and open dialogue
            for (LatLng ll : mLigamentMarkerVenue.keySet()) {
                if (ll.equals(marker.getPosition())) {
                    requestActivityToShowDialogue(mLigamentMarkerVenue.get(ll));
                }
            }

            // Event was handled by our code do not launch default behaviour.
            return true;
        }
    };

    private GoogleMap.OnMyLocationButtonClickListener mCustomOnMyLocationButtonClickListener = new GoogleMap.OnMyLocationButtonClickListener() {
        @Override
        public boolean onMyLocationButtonClick() {
            LatLng currentLocation;

            if (((MyCustomAppCompatActivity) getActivity()).isGotGpsLocation()) {
                currentLocation = ((MyCustomAppCompatActivity) getActivity()).getGpsLocation();
                panMapCameraTo(currentLocation);
            } else {
                currentLocation = null;
            }
            return true;
        }
    };


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        Log.d(TAG, "onCreateView: ");
        View view = inflater.inflate(R.layout.fragment_map, container, false);
        ((Button) getActivity().findViewById(R.id.mainMapButton)).setBackgroundColor(ContextCompat.getColor(getContext(), R.color.selectedButton));

        mLayoutInflater = (LayoutInflater) this.getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        mMapView = (MapView) view.findViewById(R.id.mapView);
        mMapView.onCreate(savedInstanceState);

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        mMapView.onResume(); // needed to get the map to display immediately

        try {
            MapsInitializer.initialize(getActivity().getApplicationContext());
        } catch (Exception e) {
            e.printStackTrace();
        }

        mMapView.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(GoogleMap mMap) {
                Log.d(TAG, "onMapReady: ");
                mGoogleMap = mMap;

                if (ActivityCompat.checkSelfPermission(MapFragment.this.getActivity(), android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                        && ActivityCompat.checkSelfPermission(MapFragment.this.getActivity(), android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

                    return;
                }
                mGoogleMap.setMyLocationEnabled(true);
                setLocationToCurrentCoordinates();
                mGoogleMap.setOnCameraChangeListener(mCustomOnCameraChangeListener);

                mGoogleMap.setOnMyLocationButtonClickListener(mCustomOnMyLocationButtonClickListener);

                mGoogleMap.setOnMarkerClickListener(mCustomOnMarkerClickListener);
            }
        });

    }

    public void requestActivityToShowDialogue(VenueModel vm) {
        ((MyCustomAppCompatActivity) getActivity()).showInfoDialogue(vm);
    }

    private void setLocationToCurrentCoordinates() {
        Log.d(TAG, "setLocationToCurrentCoordinates: ");
//        mGoogleMap.setOnCameraChangeListener(null);
        LatLng currentLocation;

        if (((MyCustomAppCompatActivity) getActivity()).isGotLocation()) {
            currentLocation = ((MyCustomAppCompatActivity) getActivity()).getLocation();
        } else {
            currentLocation = null;
        }
        panMapCameraTo(currentLocation);
    }

    private void panMapCameraTo(LatLng currentLocation) {
        if (currentLocation != null) {
            Log.d(TAG, "setLocationToCurrentCoordinates: " + String.valueOf(currentLocation.latitude) + " " + String.valueOf(currentLocation.longitude));
//            CameraPosition cameraPosition = new CameraPosition.Builder().target(currentLocation).zoom(Constants.INITIAL_MAP_ZOOM_LEVEL).build();
            try {
                if (mGoogleMap != null) {
                    mGoogleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(currentLocation, Constants.INITIAL_MAP_ZOOM_LEVEL));
                }
            } catch (NullPointerException e) {
                Log.d(TAG, "setLocationToCurrentCoordinates: Problem with google Maps");
            }
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        mMapView.onPause();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mMapView.onDestroy();
    }

    private void addMarkersToMap(ArrayList<VenueModel> venuesList) {
        Log.d(TAG, "AddMarkersToMap: ");
        try {
            mGoogleMap.clear();
        } catch (NullPointerException e) {
            Log.d(TAG, "AddMarkersToMap: problem with google maps - map isn't initialized");
            return;
        }

        mLigamentMarkerVenue = new HashMap();

        for (VenueModel venue : venuesList) {
            LatLng markerLatLng = new LatLng(venue.getLat(), venue.getLng());

            mLigamentMarkerVenue.put(markerLatLng, venue);

            MarkerOptions markerOptions = new MarkerOptions().zIndex(venue.isHighestRate() ? Float.MAX_VALUE : 0)
                    .position(markerLatLng)
                    .title(venue.getName())
                    .snippet(String.valueOf(venue.getDistance()));
            int markerDrawable = venue.isHighestRate() ? R.drawable.ic_pin_map_red : R.drawable.ic_pin_map_white;
            mGoogleMap.addMarker(markerOptions).setIcon((BitmapDescriptorFactory.fromBitmap(getMarkerBitmapFromView(markerDrawable))));
//            BitmapDescriptor markerIcon = BitmapDescriptorFactory.defaultMarker(venue.isHighestRate() ? BitmapDescriptorFactory.HUE_RED : BitmapDescriptorFactory.HUE_GREEN);
//            mGoogleMap.addMarker(markerOptions).setIcon((markerIcon));

        }
    }

    private Bitmap getMarkerBitmapFromView(@DrawableRes int resId) {
        View customMarkerView = (mLayoutInflater).inflate(R.layout.view_custom_marker, null);
        ((ImageView) customMarkerView.findViewById(R.id.custom_marker_view_dravable_container)).setImageResource(resId);
        ImageView markerImageView = (ImageView) customMarkerView.findViewById(R.id.marker_image);
        markerImageView.setImageResource(R.drawable.ic_avatar_business);
        customMarkerView.measure(View.MeasureSpec.UNSPECIFIED, View.MeasureSpec.UNSPECIFIED);
        customMarkerView.layout(0, 0, customMarkerView.getMeasuredWidth(), customMarkerView.getMeasuredHeight());
        customMarkerView.buildDrawingCache();
        Bitmap returnedBitmap = Bitmap.createBitmap(customMarkerView.getMeasuredWidth(), customMarkerView.getMeasuredHeight(),
                Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(returnedBitmap);
        canvas.drawColor(Color.WHITE, PorterDuff.Mode.SRC_IN);
        Drawable drawable = customMarkerView.getBackground();
        if (drawable != null)
            drawable.draw(canvas);
        customMarkerView.draw(canvas);
        return returnedBitmap;
    }

    @Override
    public void onDestroy() {
        Log.d(TAG, "onDestroy: ");
        super.onDestroy();
        ((Button) getActivity().findViewById(R.id.mainMapButton)).setBackgroundColor(ContextCompat.getColor(getContext(), R.color.unSelectedButton));
    }

    @Override
    public void showVenues(ArrayList<VenueModel> venuesList) {
        Log.d(TAG, "showVenues: " + venuesList.size());
        addMarkersToMap(venuesList);
    }

    @Override
    public void initialRequestData() {
        Log.d(TAG, "initialRequestData: ");
//        LatLng currentLocation = new LatLng(((MyCustomAppCompatActivity) getActivity()).getLocation().getLatitude(), ((MyCustomAppCompatActivity) getActivity()).getLocation().getLongitude());
//        mGoogleMap.setOnCameraChangeListener(null);
//        setLocationToCurrentCoordinates();
//        mGoogleMap.setOnCameraChangeListener(mCustomOnCameraChangeListener);
        if (mMapCenter != null) {
            ((MyCustomAppCompatActivity) getActivity()).makeFourSquareRequest(mMapCenter.latitude, mMapCenter.longitude, -1, mMapRadius);
        } else {
            Toast.makeText(getContext(), "Unfortunately, map isn't ready", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onLocationChanged() {
        Log.d(TAG, "onLocationChanged: ");
        setLocationToCurrentCoordinates();
    }
}
