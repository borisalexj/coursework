package com.borisalexj.coursework.ui;

import com.borisalexj.coursework.model.VenueModel;
import com.google.android.gms.maps.model.LatLng;

/**
 * Created by user on 4/18/2017.
 */

public interface InterfaceForActivity {
    public void makeFourSquareRequest(double latitude, double longitude, int _limit, int _radius);

    public boolean isGotGpsLocation();

    public boolean isGotLocation();

    public LatLng getLocation();

    public LatLng getGpsLocation();

    public void showInfoDialogue(VenueModel vm);
}
