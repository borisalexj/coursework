package com.borisalexj.coursework.ui;

import com.borisalexj.coursework.model.VenueModel;

import java.util.ArrayList;

/**
 * Created by user on 4/16/2017.
 */

public interface IntefaceForFragments {
    public void showVenues(ArrayList<VenueModel> venuesList);

    public void initialRequestData();

    public void onLocationChanged();

}
