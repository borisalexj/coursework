package com.borisalexj.coursework.orm;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.provider.BaseColumns;
import android.util.Log;

import com.borisalexj.coursework.utils.Info;

/**
 * Created by user on 4/23/2017.
 */

public class VenueDatabase extends SQLiteOpenHelper {
    String TAG = Info.TAG + this.getClass().getSimpleName();

    public VenueDatabase(Context context) {
        super(context, DatabaseContract.DB_NAME, null, DatabaseContract.DB_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        Log.d(TAG, "onCreate: " + DatabaseContract.CREATE_DATA_TABLE_QUERY);
        Log.d(TAG, "onCreate: " + DatabaseContract.CREATE_RATE_TABLE_QUERY);
        Log.d(TAG, "onCreate: " + DatabaseContract.CREATE_COMPOSITE_VIEW);
        db.execSQL(DatabaseContract.CREATE_DATA_TABLE_QUERY);
        db.execSQL(DatabaseContract.CREATE_RATE_TABLE_QUERY);
        db.execSQL(DatabaseContract.CREATE_COMPOSITE_VIEW);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {

    }

    public static final class DatabaseContract {
        public static final int DB_VERSION = 7;
        public static final String DB_NAME = "venue_db";

        public static final String DATA_TABLE_NAME = "venue_data";
        public static final String RATE_TABLE_NAME = "venue_rate";
        public static final String COMPOSITE_VIEW_NAME = "composite_view";

        public static final String CREATE_DATA_TABLE_QUERY = "create table " + DATA_TABLE_NAME + " (" +
                Columns._ID + " integer primary key autoincrement," +
                Columns.COL_idVenue + " text unique not null," +
                Columns.COL_name + " text," +
                Columns.COL_formattedPhone + " text," +
                Columns.COL_address + " text," +
                Columns.COL_lat + " real," +
                Columns.COL_lng + " real," +
                Columns.COL_price + " text," +
                Columns.COL_customUserRate + " text)";

        public static final String CREATE_RATE_TABLE_QUERY = "create table " + RATE_TABLE_NAME + " (" +
                Columns._ID + " integer primary key autoincrement," +
                Columns.COL_idVenue + " text unique not null," +
                Columns.COL_customUserRate + " integer)";

        public static final String CREATE_COMPOSITE_VIEW = "create view " + COMPOSITE_VIEW_NAME + " as select " +
                DATA_TABLE_NAME + "." + Columns.COL_idVenue + ", " +
                DATA_TABLE_NAME + "." + Columns.COL_name + ", " +
                DATA_TABLE_NAME + "." + Columns.COL_formattedPhone + ", " +
                DATA_TABLE_NAME + "." + Columns.COL_address + ", " +
                DATA_TABLE_NAME + "." + Columns.COL_lng + ", " +
                DATA_TABLE_NAME + "." + Columns.COL_lat + ", " +
                DATA_TABLE_NAME + "." + Columns.COL_price + ", " +
                RATE_TABLE_NAME + "." + Columns.COL_customUserRate +
                " from " + DATA_TABLE_NAME + " left outer join " + RATE_TABLE_NAME + " on " +
                DATA_TABLE_NAME + "." + Columns.COL_idVenue + " = " + RATE_TABLE_NAME + "." + Columns.COL_idVenue + "";

        public static class Columns implements BaseColumns {

            public static final String COL_idVenue = "idVenue";
            public static final String COL_name = "name";
            public static final String COL_formattedPhone = "formattedPhone";
            public static final String COL_address = "address";
            public static final String COL_lat = "lat";
            public static final String COL_lng = "lng";
            public static final String COL_price = "price";
            public static final String COL_customUserRate = "customUserRate";
        }

    }
}