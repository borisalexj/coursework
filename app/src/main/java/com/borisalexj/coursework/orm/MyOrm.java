package com.borisalexj.coursework.orm;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DataSetObserver;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.borisalexj.coursework.model.VenueModel;
import com.borisalexj.coursework.orm.VenueDatabase.DatabaseContract;
import com.borisalexj.coursework.utils.Info;
import com.google.android.gms.maps.model.LatLng;

import java.util.ArrayList;
import java.util.HashMap;


/**
 * Created by user on 4/23/2017.
 */

public class MyOrm {
    private String TAG = Info.TAG + this.getClass().getSimpleName();
    private VenueDatabase mVenueDatabase;
    private SQLiteDatabase mSqLiteVenueDb;
    private HashMap<String, Integer> mRatesArray;

    public MyOrm(Context context) {
        Log.d(TAG, "MyOrm: ");
        mVenueDatabase = new VenueDatabase(context);
        mRatesArray = readAllRates();

        Cursor cursor = mVenueDatabase.getReadableDatabase().query(VenueDatabase.DatabaseContract.RATE_TABLE_NAME,
                null, null, null, null, null, null);
//        cursor.registerContentObserver();
        cursor.registerDataSetObserver(new DataSetObserver() {
            @Override
            public void onChanged() {
                Log.d(TAG, "onChanged: registerDataSetObserver");
                super.onChanged();
                mRatesArray = readAllRates();
            }
        });

    }

    public HashMap<String, Integer> getRatesArray() {
        return mRatesArray;
    }

    public void storeVenues(ArrayList<VenueModel> venuesArray) {
        Log.d(TAG, "storeVenues: " + venuesArray.size());
        mSqLiteVenueDb = mVenueDatabase.getWritableDatabase();

        try {
            for (int i = 0; i < venuesArray.size(); i++) {
                Log.d(TAG, "storeVenues: " + venuesArray.get(i));
                mSqLiteVenueDb.insertWithOnConflict(VenueDatabase.DatabaseContract.DATA_TABLE_NAME, null, venuesArray.get(i).toDb(), SQLiteDatabase.CONFLICT_REPLACE);
            }
        } catch (Exception e) {
            Log.d(TAG, "storeItems: ");
            e.printStackTrace();
        } finally {
            mSqLiteVenueDb.close();
        }

    }

    public ArrayList<VenueModel> readAllVenues() {
        Log.d(TAG, "readAllVenues: ");
        ArrayList<VenueModel> venuesArray = new ArrayList<>();
        mSqLiteVenueDb = mVenueDatabase.getReadableDatabase();

        Cursor cursor = mSqLiteVenueDb.query(VenueDatabase.DatabaseContract.COMPOSITE_VIEW_NAME,
                null, null, null, null, null, null);
        Log.d(TAG, "readAllVenues: cursor_size - " + String.valueOf(cursor.getCount()));
        cursor.moveToFirst();
        if (cursor.getCount() != 0) {
            do {
                venuesArray.add(new VenueModel(
                        cursor.getString(cursor.getColumnIndex(DatabaseContract.Columns.COL_idVenue)),
                        cursor.getString(cursor.getColumnIndex(DatabaseContract.Columns.COL_name)),
                        cursor.getString(cursor.getColumnIndex(DatabaseContract.Columns.COL_formattedPhone)),
                        cursor.getString(cursor.getColumnIndex(DatabaseContract.Columns.COL_address)),
                        cursor.getDouble(cursor.getColumnIndex(DatabaseContract.Columns.COL_lat)),
                        cursor.getDouble(cursor.getColumnIndex(DatabaseContract.Columns.COL_lng)),
                        cursor.getString(cursor.getColumnIndex(DatabaseContract.Columns.COL_price)),
                        cursor.getInt(cursor.getColumnIndex(DatabaseContract.Columns.COL_customUserRate))
                ));
            } while (cursor.moveToNext());
            Log.d(TAG, "readAllVenues: array_size - " + String.valueOf(venuesArray.size()));
        }
        mSqLiteVenueDb.close();

        return venuesArray;
    }

    public HashMap<String, Integer> readAllRates() {
        Log.d(TAG, "readAllRates: ");
        HashMap<String, Integer> ratesArray = new HashMap<>();
        mSqLiteVenueDb = mVenueDatabase.getReadableDatabase();

        Cursor cursor = mSqLiteVenueDb.query(VenueDatabase.DatabaseContract.RATE_TABLE_NAME,
                null, null, null, null, null, null);

        cursor.moveToFirst();
        Log.d(TAG, "readAllRates: " + cursor.getCount());

        if (cursor.getCount() != 0) {
            do {
                ratesArray.put(
                        cursor.getString(cursor.getColumnIndex(DatabaseContract.Columns.COL_idVenue)),
                        cursor.getInt(cursor.getColumnIndex(DatabaseContract.Columns.COL_customUserRate)
                        ));
            } while (cursor.moveToNext());
            Log.d(TAG, "readAllVenues: array_size - " + String.valueOf(ratesArray.size()));
        }
        mSqLiteVenueDb.close();

        return ratesArray;
    }

    public ArrayList<VenueModel> readVenuesInBounds(LatLng sw, LatLng ne) {
        Log.d(TAG, "readVenuesInBounds: ");
        ArrayList<VenueModel> venuesArray = new ArrayList<>();
        mSqLiteVenueDb = mVenueDatabase.getReadableDatabase();

        String selection = DatabaseContract.Columns.COL_lat + " < ? AND " + DatabaseContract.Columns.COL_lat + " > ? and " + DatabaseContract.Columns.COL_lng + "  > ? AND " + DatabaseContract.Columns.COL_lng + " < ? ";
        String[] selectionArgs = new String[]{String.valueOf(ne.latitude), String.valueOf(sw.latitude), String.valueOf(ne.longitude), String.valueOf(sw.longitude)};

        Cursor cursor = mSqLiteVenueDb.query(VenueDatabase.DatabaseContract.DATA_TABLE_NAME,
                null, selection, selectionArgs, null, null, null);
        cursor.moveToFirst();
        String venueId = "";
        if (cursor.getCount() != 0) {
            do {
                venueId = cursor.getString(cursor.getColumnIndex(DatabaseContract.Columns.COL_idVenue));
                venuesArray.add(new VenueModel(
                        venueId,
                        cursor.getString(cursor.getColumnIndex(DatabaseContract.Columns.COL_name)),
                        cursor.getString(cursor.getColumnIndex(DatabaseContract.Columns.COL_formattedPhone)),
                        cursor.getString(cursor.getColumnIndex(DatabaseContract.Columns.COL_address)),
                        cursor.getDouble(cursor.getColumnIndex(DatabaseContract.Columns.COL_lat)),
                        cursor.getDouble(cursor.getColumnIndex(DatabaseContract.Columns.COL_lng)),
                        cursor.getString(cursor.getColumnIndex(DatabaseContract.Columns.COL_price)),
                        mRatesArray.containsKey(venueId) ? mRatesArray.get(venueId) : 0
                ));
            } while (cursor.moveToNext());
            Log.d(TAG, "readVenuesInBounds: array_size - " + String.valueOf(venuesArray.size()));
        }
        mSqLiteVenueDb.close();

        return venuesArray;
    }

    public void updateUserCustomRate(String idVenue, String newRate) {
        Log.d(TAG, "updateUserCustomRate: ");
        ContentValues contentValues = new ContentValues();
        contentValues.put(DatabaseContract.Columns.COL_idVenue, idVenue);
        contentValues.put(DatabaseContract.Columns.COL_customUserRate, newRate);

        mSqLiteVenueDb = mVenueDatabase.getWritableDatabase();

        try {
            mSqLiteVenueDb.insertWithOnConflict(DatabaseContract.RATE_TABLE_NAME, null, contentValues, SQLiteDatabase.CONFLICT_REPLACE);
        } catch (Exception e) {
            Log.d(TAG, "updateUserCustomRate: ");
            e.printStackTrace();
        } finally {
            mSqLiteVenueDb.close();
        }
        mRatesArray = readAllRates();
    }

    public ArrayList<VenueModel> testView() {
        ArrayList<VenueModel> venuesArray = new ArrayList<>();
        mSqLiteVenueDb = mVenueDatabase.getReadableDatabase();

        Cursor cursor = mSqLiteVenueDb.query(VenueDatabase.DatabaseContract.COMPOSITE_VIEW_NAME,
                null, null, null, null, null, null);
        Log.d(TAG, "readAllVenues: cursor_size - " + String.valueOf(cursor.getCount()));
        cursor.moveToFirst();
        String venueId = "";
        if (cursor.getCount() != 0) {
            do {
                venueId = cursor.getString(cursor.getColumnIndex(DatabaseContract.Columns.COL_idVenue));
                venuesArray.add(new VenueModel(
                        venueId,
                        cursor.getString(cursor.getColumnIndex(DatabaseContract.Columns.COL_name)),
                        cursor.getString(cursor.getColumnIndex(DatabaseContract.Columns.COL_formattedPhone)),
                        cursor.getString(cursor.getColumnIndex(DatabaseContract.Columns.COL_address)),
                        cursor.getDouble(cursor.getColumnIndex(DatabaseContract.Columns.COL_lat)),
                        cursor.getDouble(cursor.getColumnIndex(DatabaseContract.Columns.COL_lng)),
                        cursor.getString(cursor.getColumnIndex(DatabaseContract.Columns.COL_price)),
                        cursor.getInt(cursor.getColumnIndex(DatabaseContract.Columns.COL_customUserRate))
                ));
            } while (cursor.moveToNext());
            Log.d(TAG, "readAllVenues: array_size - " + String.valueOf(venuesArray.size()));
        }
        mSqLiteVenueDb.close();

        for (VenueModel vm : venuesArray) {
            Log.d(TAG, "testView: " + vm);
        }

        return venuesArray;
    }
}
