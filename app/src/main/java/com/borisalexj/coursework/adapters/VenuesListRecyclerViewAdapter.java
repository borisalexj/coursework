package com.borisalexj.coursework.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.borisalexj.coursework.R;
import com.borisalexj.coursework.model.VenueModel;
import com.borisalexj.coursework.utils.Info;

import java.util.ArrayList;

/**
 * Created by user on 4/16/2017.
 */
public class VenuesListRecyclerViewAdapter extends RecyclerView.Adapter<VenuesListRecyclerViewAdapter.ViewHolder> {
    private String TAG = Info.TAG + this.getClass().getSimpleName();
    private ArrayList<VenueModel> mVenuesList;
    private Context mContext;
    private Callback callBackViewHolder = new Callback() {
    };

    public VenuesListRecyclerViewAdapter(Context context, ArrayList<VenueModel> venuesList) {
        Log.d(TAG, "VenuesListRecyclerViewAdapter: ");
        mVenuesList = venuesList;
        mContext = context;
    }

    public void setVenuesList(ArrayList<VenueModel> venuesList) {
        mVenuesList = venuesList;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Log.d(TAG, "onCreateViewHolder: " + viewType);
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.venue_list_item, parent, false);
        ViewHolder viewHolder = new ViewHolder(callBackViewHolder, view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Log.d(TAG, "onBindViewHolder: " + position);
        holder.setNameDistance(mVenuesList.get(position).getName(), String.valueOf(mVenuesList.get(position).getDistance()));

    }

    @Override
    public int getItemCount() {
//        Log.d(TAG, "getItemCount: " + mVenuesList.size());
        return mVenuesList == null ? 0 : mVenuesList.size();
    }

    public interface Callback {

    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView venueNameTextView;
        TextView distanceToVenueTextView;
        private String TAG = Info.TAG + this.getClass().getSimpleName();
        private Context mContext;

        public ViewHolder(Callback callback, View itemView) {
            super(itemView);
            mContext = itemView.getContext();
            venueNameTextView = (TextView) itemView.findViewById(R.id.venueNameTextView);
            distanceToVenueTextView = (TextView) itemView.findViewById(R.id.distanceToVenueTextView);
//            getPosition();
            Log.d(TAG, "ViewHolder: " + getPosition());
            Log.d(TAG, "ViewHolder: " + getAdapterPosition());
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Log.d(TAG, "onClick: " + getAdapterPosition());
                    Toast.makeText(mContext, String.valueOf(getAdapterPosition()) + "\n dialog will be here", Toast.LENGTH_LONG).show();

                }
            });
        }

        public void setNameDistance(String venueName, String distanceToVenue) {
            venueNameTextView.setText(venueName);
            distanceToVenueTextView.setText(distanceToVenue);
        }
    }
}
