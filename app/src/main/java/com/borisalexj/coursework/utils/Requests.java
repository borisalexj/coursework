package com.borisalexj.coursework.utils;

/**
 * Created by user on 3/25/2017.
 */

public class Requests {
    public static final int REQUEST_LOCATION_PERMISSION = 11;
    public static final int REQUEST_LOCATION_TURN_ON = 22;
    public static final int PLACE_AUTOCOMPLETE_REQUEST_CODE = 33;
}
