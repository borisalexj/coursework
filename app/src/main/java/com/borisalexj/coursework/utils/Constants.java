package com.borisalexj.coursework.utils;

/**
 * Created by user on 4/18/2017.
 */
public class Constants {
    public static final int RADIUS = 3000;
    public static final int START_ITEM_COUNT = 10;

    public static final int INITIAL_MAP_ZOOM_LEVEL = 14;
    public static final int LOCATION_INTERVAL = 60;
    public static final int LOCATION_DISTANCE = 50;

    public static final int MSG_START_GEOLOCATION = 1;
    public static final String INTENT_GOT_LOCATION = "INTENT_GOT_LOCATION";

    public static class HTTP {
        public static final String foursquare_client_id = "HAK42RSPGXIHSGAGKGJ3YGHEAWUKQGD0X51XRAOP0GI1WALK";
        public static final String foursquare_client_secret = "5DUK2TZOOFWRJWGZLLB4SXPQTVTPYTH4ARXFA2RLVJGWD24V";
        public static final String foursquare_v = "20131016";
        public static final String foursquare_intent = "browse";
        public static final String foursquare_category_id = "4d4b7105d754a06374d81259";
        public static final String foursquare_m = "foursquare";

        public static final String foursquare_api_base_url = "https://api.foursquare.com/";
        public static final String foursquare_api_method_venues_search = "/v2/venues/search";
        public static final String foursquare_api_method_venue_details = "/v2/venues/";

        public static final String ll = String.valueOf("50.22" + "," + "22.22");
        public static final String limit = "10";
        public static final String radius = "3000";

//    private static final String FEED_URL = foursquare_api_base_url +
//
//            foursquare_api_method_venues_search + "?" +
//            "v=" + foursquare_v + "&" +
//            "categoryId=" + foursquare_category_id + "&" +
//            "client_id=" + foursquare_client_id + "&" +
//            "client_secret=" + foursquare_client_secret + "&" +
//            "ll=" + ll + "&" +
//            "limit=" + limit + "&" +
//            "radius=" + radius + "&" +
//            "m=" + foursquare_m;

    }
}
