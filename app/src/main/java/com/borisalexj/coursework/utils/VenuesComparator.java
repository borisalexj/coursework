package com.borisalexj.coursework.utils;

import com.borisalexj.coursework.model.VenueModel;

import java.util.Comparator;

/**
 * Created by user on 4/23/2017.
 */

public class VenuesComparator implements Comparator<VenueModel> {
    private String TAG = Info.TAG + this.getClass().getSimpleName();

    @Override
    public int compare(VenueModel o1, VenueModel o2) {
        if (o1.getDistance() > o2.getDistance()) {
            return 1;
        } else if (o1.getDistance() < o2.getDistance()) {
            return -1;
        } else {
            return 0;
        }

    }
}