package com.borisalexj.coursework.utils;

import android.util.Log;

import com.borisalexj.coursework.model.VenueModel;
import com.google.android.gms.maps.model.LatLng;
import com.google.maps.android.SphericalUtil;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Reader;
import java.util.ArrayList;
import java.util.HashMap;

import okhttp3.ResponseBody;
import retrofit2.Response;

/**
 * Created by user on 5/8/2017.
 */

public class StaticHelpers {
    private static String TAG = Info.TAG + "StaticHelpers";

    public static JSONObject responseToJson(Response<ResponseBody> response) {
        Log.d(TAG, "responseToJson: ");
        Reader reader = response.body().charStream();
        StringBuilder sb = new StringBuilder();
        char[] buffer = new char[8196];
        try {
            while (reader.read(buffer) != -1) {
                sb.append(buffer);
            }
        } catch (Exception e) {
            Log.e(TAG, "I/O error while convering response to JSON", e);
        }
        JSONObject result = null;
        try {
            result = new JSONObject(sb.toString());
        } catch (JSONException e) {
            Log.e(TAG, "Error converting response to JSON", e);
        }
        Log.d(TAG, "responseToJson: " + result);
        return result;
    }

    public static ArrayList<VenueModel> subList(ArrayList<VenueModel> venues, int limit) {
        Log.d(TAG, "subList: ");
        ArrayList<VenueModel> venuesResult = new ArrayList<>();
        for (int i = 0; i < limit; i++) {
            venuesResult.add(venues.get(i));
        }
        return venuesResult;
    }

    public static ArrayList<VenueModel> weedWithinRadius(ArrayList<VenueModel> venues, int radius) {
        Log.d(TAG, "weedWithinRadius: ");
        ArrayList<VenueModel> venuesResult = new ArrayList<>();
        if (venues.size() == 0) {
            return venuesResult;
        }
        for (VenueModel vm : venues) {
            if (vm.getDistance() <= radius) {
                venuesResult.add(vm);
            }
        }
        return venuesResult;
    }

    public static ArrayList<VenueModel> markHighestRated(ArrayList<VenueModel> venues) {
        Log.d(TAG, "markHighestRated: ");
        int rate = 0;
        if (venues.size() == 0) {
            ArrayList<VenueModel> venuesResult = new ArrayList<>();
            return venuesResult;
        }
        for (VenueModel vm : venues) {
            if (vm.getCustomUserRate() > rate) {
                rate = vm.getCustomUserRate();
            }
        }
        if (rate != 0) {
            for (VenueModel vm : venues) {
                if (vm.getCustomUserRate() == rate) {
                    vm.setHighestRate(true);
                }
            }
        }
        return venues;
    }

    public static ArrayList<VenueModel> recalculateDistances(ArrayList<VenueModel> venues, LatLng latLng) {
        Log.d(TAG, "recalculateDistances: ");
        for (VenueModel venue : venues) {
            venue.setDistance((int) Math.round(SphericalUtil.computeDistanceBetween(latLng, venue.getLatLng())));
        }
        return venues;
    }

    public static ArrayList<VenueModel> setCustomRates(ArrayList<VenueModel> venues, HashMap<String, Integer> ratesArray) {
        Log.d(TAG, "setCustomRates: ");
//        ArrayList<VenueModel> venues = new ArrayList<>();
        // Todo       HashMap<String, Integer> ratesArray = mMyOrm.getRatesArray();
        for (VenueModel vms : venues) {
            vms.setCustomUserRate(ratesArray.containsKey(vms.getIdVenue()) ? ratesArray.get(vms.getIdVenue()) : 0);
        }
        return venues;
    }

    public static ArrayList<VenueModel> parseVenuesSearchResponse(JSONObject result) throws JSONException {
        Log.d(TAG, "parseVenuesSearchResponse: ");
        ArrayList<VenueModel> venues = new ArrayList<>();
        JSONArray resultArray = result.getJSONObject("response").getJSONArray("venues");
        for (int i = 0; i < resultArray.length(); i++) {
            VenueModel vms = new VenueModel();
            JSONObject o = (JSONObject) resultArray.get(i);
            if (o.has("id")) {
                vms.setIdVenue(o.getString("id"));
            }
            if (o.has("name")) {
                vms.setName(o.getString("name"));
            }

            if (o.has("formattedPhone")) {
                vms.setFormattedPhone(o.getJSONObject("contact").getString("formattedPhone"));
            }
            JSONObject l = o.getJSONObject("location");
            if (l.has("address")) {
                vms.setAddress(l.getString("address"));
            }
            if (l.has("lat")) {
                vms.setLat(l.getDouble("lat"));
            }
            if (l.has("lng")) {
                vms.setLng(l.getDouble("lng"));
            }
            if (l.has("distance")) {
                vms.setDistance(l.getInt("distance"));
            }
            venues.add(vms);
        }
        return venues;
    }
}
