package com.borisalexj.coursework.model;

import android.content.ContentValues;

import com.borisalexj.coursework.orm.VenueDatabase;
import com.google.android.gms.maps.model.LatLng;

/**
 * Created by user on 3/14/2017.
 */

public class VenueModel {


    private String idVenue = "";
    private String name = "";
    private String formattedPhone = "";
    private String address = "";
    private double lat = 0;
    private double lng = 0;
    private int distance = 0;
    private String price = "";
    private int customUserRate = 0;
    private boolean highestRate = false;

    public VenueModel() {
    }

    public VenueModel(String idVenue, String name, String formattedPhone, String address, double lat, double lng, String price, int customUserRate) {
        this.idVenue = idVenue;
        this.name = name;
        this.formattedPhone = formattedPhone;
        this.address = address;
        this.lat = lat;
        this.lng = lng;
        this.price = price;
        this.customUserRate = customUserRate;
    }

    public ContentValues toDb() {
        ContentValues contentValues = new ContentValues();
        contentValues.put(VenueDatabase.DatabaseContract.Columns.COL_idVenue, idVenue);
        contentValues.put(VenueDatabase.DatabaseContract.Columns.COL_name, name);
        contentValues.put(VenueDatabase.DatabaseContract.Columns.COL_formattedPhone, formattedPhone);
        contentValues.put(VenueDatabase.DatabaseContract.Columns.COL_address, address);
        contentValues.put(VenueDatabase.DatabaseContract.Columns.COL_lat, lat);
        contentValues.put(VenueDatabase.DatabaseContract.Columns.COL_lng, lng);
        contentValues.put(VenueDatabase.DatabaseContract.Columns.COL_price, price);
        return contentValues;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public boolean isHighestRate() {

        return highestRate;
    }

    public void setHighestRate(boolean highestRate) {
        this.highestRate = highestRate;
    }

    public int getCustomUserRate() {
        return customUserRate;
    }

    public void setCustomUserRate(int customUserRate) {
        this.customUserRate = customUserRate;
    }

    public String getIdVenue() {
        return idVenue;
    }

    public void setIdVenue(String idVenue) {
        this.idVenue = idVenue;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFormattedPhone() {
        return formattedPhone;
    }

    public void setFormattedPhone(String formattedPhone) {
        this.formattedPhone = formattedPhone;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public double getLng() {
        return lng;
    }

    public void setLng(double lng) {
        this.lng = lng;
    }

    public double getDistance() {
        return Math.round(distance);
    }

    public void setDistance(int distance) {
        this.distance = distance;
    }

    @Override
    public String toString() {
        return "VenueModel{" +
                "idVenue='" + idVenue + '\'' +
                ", name='" + name + '\'' +
                ", formattedPhone='" + formattedPhone + '\'' +
                ", address='" + address + '\'' +
                ", lat=" + lat +
                ", lng=" + lng +
                ", distance=" + distance +
                ", price='" + price + '\'' +
                ", customUserRate=" + customUserRate +
                ", highestRate=" + highestRate +
                '}';
    }

    public LatLng getLatLng() {

        return new LatLng(this.lat, this.lng);
    }
}
