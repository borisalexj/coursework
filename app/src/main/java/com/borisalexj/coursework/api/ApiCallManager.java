package com.borisalexj.coursework.api;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import com.borisalexj.coursework.BuildConfig;
import com.borisalexj.coursework.model.VenueModel;
import com.borisalexj.coursework.utils.Constants;
import com.borisalexj.coursework.utils.Info;
import com.borisalexj.coursework.utils.StaticHelpers;

import org.json.JSONException;
import org.json.JSONObject;

import java.security.InvalidParameterException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutionException;

/**
 * Created by user on 5/8/2017.
 */

public class ApiCallManager {
    String TAG = Info.TAG + this.getClass().getSimpleName();

    Context mContext;

    public ApiCallManager(Context mContext) {
        this.mContext = mContext;
    }

    public ArrayList<VenueModel> GetVenues(String ll, String limit, String radius) {
        String methodName = "GetVenues - ";

        if (limit == null && radius == null) {
            throw new InvalidParameterException("Either \"limit\" or \"radius\" must be set");
        }

        ApiGetSearchVenues getThread = new ApiGetSearchVenues();
        ArrayList<VenueModel> venues;
        Map<String, String> inMap = new HashMap<>();
        inMap.put("url", Constants.HTTP.foursquare_api_method_venues_search);
        inMap.put("ll", ll);

        if (limit != null) {
            inMap.put("limit", limit);
        }
        if (radius != null) {
            inMap.put("radius", radius);
        }


        getThread.execute(inMap);

        try {
            JSONObject result = getThread.get();
            venues = StaticHelpers.parseVenuesSearchResponse(result);
            if (BuildConfig.DEBUG) {
                Log.d(TAG, methodName + String.valueOf(result));
            }
            return venues;
        } catch (JSONException | InterruptedException | ExecutionException | NullPointerException e) {
            e.printStackTrace();
            Log.d(TAG, "check_internet_connection");
            Toast.makeText(mContext, "check_internet_connection", Toast.LENGTH_SHORT).show();
            return null;
        }

    }

    public String GetVenueDetail(String idVenue) {
        String methodName = "GetVenues - ";

        ApiGetSearchVenues getThread = new ApiGetSearchVenues();
        String venuePrice;
        Map<String, String> inMap = new HashMap<>();
        inMap.put("url", Constants.HTTP.foursquare_api_method_venue_details + idVenue);

        getThread.execute(inMap);

        try {
            JSONObject result = getThread.get();

            venuePrice = String.valueOf(result.getJSONObject("response").getJSONObject("venue").getJSONObject("price").getInt("tier"));
            if (BuildConfig.DEBUG) {
                Log.d(TAG, methodName + String.valueOf(result));
            }
            return venuePrice;
        } catch (JSONException | InterruptedException | ExecutionException | NullPointerException e) {
            e.printStackTrace();
            Log.d(TAG, "check_internet_connection");
            Toast.makeText(mContext, "check_internet_connection", Toast.LENGTH_SHORT).show();
            return "Price unknown";
        }

    }

}
