package com.borisalexj.coursework.api;

import com.borisalexj.coursework.utils.Constants;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;

/**
 * Created by user on 5/8/2017.
 */
public class ServiceGenerator {

    //    public static final String API_BASE_URL = "http://your.api-base.url";
//    public static final String API_BASE_URL = "https://api.github.com/";
    public static String API_BASE_URL = Constants.HTTP.foursquare_api_base_url;


    private static OkHttpClient.Builder httpClient = new OkHttpClient.Builder();

    private static Retrofit.Builder builder =
            new Retrofit.Builder()
//                    .addConverterFactory(GsonConverterFactory.create())
                    .baseUrl(API_BASE_URL);

    public static <S> S createService(Class<S> serviceClass) {
        Retrofit retrofit = builder.client(httpClient.build()).build();
        return retrofit.create(serviceClass);
    }

    public static void changeApiBaseUrl(String newApiBaseUrl) {
        API_BASE_URL = newApiBaseUrl;

        builder = new Retrofit.Builder()
//                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl(API_BASE_URL);
    }
}

//https://futurestud.io/blog/retrofit-2-how-to-change-api-base-url-at-runtime-2