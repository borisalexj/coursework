package com.borisalexj.coursework.api;

import org.json.JSONObject;

import java.util.Map;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;
import retrofit2.http.QueryMap;

/**
 * Created by user on 3/12/2017.
 */


public interface iFourSquareApi {
    @FormUrlEncoded
    @POST("{url}")
    Call<JSONObject> post(@Path(value = "url", encoded = true) String url,
                          @FieldMap Map<String, String> map);

    @GET("{url}")
    Call<ResponseBody> searchVenues(@Path(value = "url", encoded = true) String url,
                                    @QueryMap Map<String, String> options);


    @GET("{url}")
    Call<ResponseBody> searchVenuesForList(@Path(value = "url", encoded = true) String url,
                                           @Query("v") String v,
                                           @Query("categoryId") String categoryId,
                                           @Query("client_id") String client_id,
                                           @Query("client_secret") String client_secret,
                                           @Query("ll") String ll,
                                           @Query("limit") String limit,
                                           @Query("m") String m);

    @GET("{url}")
    Call<ResponseBody> searchVenuesForMap(@Path(value = "url", encoded = true) String url,
                                          @Query("v") String v,
                                          @Query("categoryId") String categoryId,
                                          @Query("client_id") String client_id,
                                          @Query("client_secret") String client_secret,
                                          @Query("ll") String ll,
                                          @Query("radius") String radius,
                                          @Query("m") String m);

    @GET("{url}")
    Call<ResponseBody> venuesDetail(@Path(value = "url", encoded = true) String url,
                                    @Query("v") String v,
                                    @Query("client_id") String client_id,
                                    @Query("client_secret") String client_secret);

}
