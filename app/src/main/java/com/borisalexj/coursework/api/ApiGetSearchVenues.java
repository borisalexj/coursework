package com.borisalexj.coursework.api;

import android.os.AsyncTask;
import android.util.Log;

import com.borisalexj.coursework.BuildConfig;
import com.borisalexj.coursework.utils.Constants;
import com.borisalexj.coursework.utils.Info;
import com.borisalexj.coursework.utils.StaticHelpers;

import org.json.JSONObject;

import java.io.IOException;
import java.util.Map;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Response;

/**
 * Created by user on 5/8/2017.
 */

public class ApiGetSearchVenues extends AsyncTask<Map<String, String>, String, JSONObject> {
    String TAG = Info.TAG + this.getClass().getSimpleName();

    @Override
    protected JSONObject doInBackground(Map<String, String>... params) {

        JSONObject result;
        iFourSquareApi client = ServiceGenerator.createService(iFourSquareApi.class);

        String url = params[0].get("url");
        params[0].remove("url");

        params[0].put("v", Constants.HTTP.foursquare_v);
        params[0].put("categoryId", Constants.HTTP.foursquare_category_id);
        params[0].put("client_id", Constants.HTTP.foursquare_client_id);
        params[0].put("client_secret", Constants.HTTP.foursquare_client_secret);
        params[0].put("m", Constants.HTTP.foursquare_m);

        Call<ResponseBody> call = client.searchVenues(url, params[0]);

        try {
            Response<ResponseBody> response = call.execute();
            result = StaticHelpers.responseToJson(response);

            if (BuildConfig.DEBUG) {
                Log.d(TAG, " - debug " + call.request().url());
            }

        } catch (IOException e) {
            Log.d(TAG, " " + call.request().url() + e.getMessage());
            e.printStackTrace();
            return null;
        }
        return result;
    }
}
